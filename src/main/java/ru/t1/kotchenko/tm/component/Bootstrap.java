package ru.t1.kotchenko.tm.component;

import ru.t1.kotchenko.tm.api.component.IBootstrap;
import ru.t1.kotchenko.tm.api.controller.ICommandController;
import ru.t1.kotchenko.tm.api.controller.IProjectController;
import ru.t1.kotchenko.tm.api.controller.ITaskController;
import ru.t1.kotchenko.tm.api.repository.ICommandRepository;
import ru.t1.kotchenko.tm.api.repository.IProjectRepository;
import ru.t1.kotchenko.tm.api.repository.ITaskRepository;
import ru.t1.kotchenko.tm.api.service.ICommandService;
import ru.t1.kotchenko.tm.api.service.IProjectService;
import ru.t1.kotchenko.tm.api.service.ITaskService;
import ru.t1.kotchenko.tm.constant.ArgumentConstant;
import ru.t1.kotchenko.tm.constant.TerminalConstant;
import ru.t1.kotchenko.tm.controller.CommandController;
import ru.t1.kotchenko.tm.controller.ProjectController;
import ru.t1.kotchenko.tm.controller.TaskController;
import ru.t1.kotchenko.tm.repository.CommandRepository;
import ru.t1.kotchenko.tm.repository.ProjectRepository;
import ru.t1.kotchenko.tm.repository.TaskRepository;
import ru.t1.kotchenko.tm.service.CommandService;
import ru.t1.kotchenko.tm.service.ProjectService;
import ru.t1.kotchenko.tm.service.TaskService;
import ru.t1.kotchenko.tm.util.TerminalUtil;

public final class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskController taskController = new TaskController(taskService);

    @Override
    public void run(final String... args) {
        parseArguments(args);
        parseCommands();
    }

    private void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArgument(arg);
    }

    private void parseCommands() {
        commandController.showWelcome();
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConstant.VERSION:
                commandController.showVersion();
                break;
            case TerminalConstant.HELP:
                commandController.showHelp();
                break;
            case TerminalConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case TerminalConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConstant.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConstant.REMOVE_PROJECT_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConstant.REMOVE_PROJECT_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConstant.SHOW_PROJECT_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConstant.SHOW_PROJECT_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConstant.UPDATE_PROJECT_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConstant.UPDATE_PROJECT_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConstant.REMOVE_TASK_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConstant.REMOVE_TASK_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConstant.SHOW_TASK_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConstant.SHOW_TASK_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConstant.UPDATE_TASK_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConstant.UPDATE_TASK_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConstant.EXIT:
                exit();
                break;
            default:
                commandController.showCommandError();
        }
    }

    private void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showArgumentError();
        }
        exit();
    }

    private void exit() {
        System.exit(0);
    }

}
