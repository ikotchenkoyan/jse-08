package ru.t1.kotchenko.tm.constant;

public final class TerminalConstant {

    public static final String INFO = "info";

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String EXIT = "exit";

    public static final String ARGUMENTS = "arguments";

    public static final String COMMANDS = "commands";

    public static final String PROJECT_LIST = "project-list";

    public static final String PROJECT_CREATE = "project-create";

    public static final String PROJECT_CLEAR = "project-clear";

    public static final String REMOVE_PROJECT_BY_ID = "project-remove-by-id";

    public static final String REMOVE_PROJECT_BY_INDEX = "project-remove-by-index";

    public static final String SHOW_PROJECT_BY_ID = "project-show-by-id";

    public static final String SHOW_PROJECT_BY_INDEX = "project-show-by-index";

    public static final String UPDATE_PROJECT_BY_ID = "project-update-by-id";

    public static final String UPDATE_PROJECT_BY_INDEX = "project-update-by-index";

    public static final String TASK_LIST = "task-list";

    public static final String TASK_CREATE = "task-create";

    public static final String TASK_CLEAR = "task-clear";

    public static final String REMOVE_TASK_BY_ID = "task-remove-by-id";

    public static final String REMOVE_TASK_BY_INDEX = "task-remove-by-index";

    public static final String SHOW_TASK_BY_ID = "task-show-by-id";

    public static final String SHOW_TASK_BY_INDEX = "task-show-by-index";

    public static final String UPDATE_TASK_BY_ID = "task-update-by-id";

    public static final String UPDATE_TASK_BY_INDEX = "task-update-by-index";

}
