package ru.t1.kotchenko.tm.repository;

import ru.t1.kotchenko.tm.api.repository.ICommandRepository;
import ru.t1.kotchenko.tm.constant.ArgumentConstant;
import ru.t1.kotchenko.tm.constant.TerminalConstant;
import ru.t1.kotchenko.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            TerminalConstant.HELP, ArgumentConstant.HELP,
            "Show application commands."
    );

    private static final Command INFO = new Command(
            TerminalConstant.INFO, ArgumentConstant.INFO,
            "Show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConstant.VERSION, ArgumentConstant.VERSION,
            "Show application version."
    );

    private static final Command COMMANDS = new Command(
            TerminalConstant.COMMANDS, ArgumentConstant.COMMANDS,
            "Show application commands."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConstant.ARGUMENTS, ArgumentConstant.ARGUMENTS,
            "Show application arguments."
    );

    private static final Command EXIT = new Command(
            TerminalConstant.EXIT, null,
            "Close application."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConstant.PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConstant.PROJECT_LIST, null,
            "Show project list."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConstant.PROJECT_CLEAR, null,
            "Remove all tasks."
    );

    private static final Command REMOVE_PROJECT_BY_ID = new Command(
            TerminalConstant.REMOVE_PROJECT_BY_ID, null,
            "Remove project by id."
    );

    private static final Command REMOVE_PROJECT_BY_INDEX = new Command(
            TerminalConstant.REMOVE_PROJECT_BY_INDEX, null,
            "Remove project by index."
    );

    private static final Command SHOW_PROJECT_BY_ID = new Command(
            TerminalConstant.SHOW_PROJECT_BY_ID, null,
            "Show project by id."
    );

    private static final Command SHOW_PROJECT_BY_INDEX = new Command(
            TerminalConstant.SHOW_PROJECT_BY_INDEX, null,
            "Show project by id."
    );

    private static final Command UPDATE_PROJECT_BY_ID = new Command(
            TerminalConstant.UPDATE_PROJECT_BY_ID, null,
            "Update project by id."
    );

    private static final Command UPDATE_PROJECT_BY_INDEX = new Command(
            TerminalConstant.UPDATE_PROJECT_BY_INDEX, null,
            "Update project by index."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConstant.TASK_CREATE, null,
            "Create new project."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConstant.TASK_LIST, null,
            "Show task list."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConstant.TASK_CLEAR, null,
            "Remove all tasks."
    );

    private static final Command REMOVE_TASK_BY_ID = new Command(
            TerminalConstant.REMOVE_TASK_BY_ID, null,
            "Remove project by id."
    );

    private static final Command REMOVE_TASK_BY_INDEX = new Command(
            TerminalConstant.REMOVE_TASK_BY_INDEX, null,
            "Remove project by index."
    );

    private static final Command SHOW_TASK_BY_ID = new Command(
            TerminalConstant.SHOW_TASK_BY_ID, null,
            "Show project by id."
    );

    private static final Command SHOW_TASK_BY_INDEX = new Command(
            TerminalConstant.SHOW_TASK_BY_INDEX, null,
            "Show project by id."
    );

    private static final Command UPDATE_TASK_BY_ID = new Command(
            TerminalConstant.UPDATE_TASK_BY_ID, null,
            "Update project by id."
    );

    private static final Command UPDATE_TASK_BY_INDEX = new Command(
            TerminalConstant.UPDATE_TASK_BY_INDEX, null,
            "Update project by index."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, INFO, VERSION, COMMANDS, ARGUMENTS,

            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            REMOVE_PROJECT_BY_ID, REMOVE_PROJECT_BY_INDEX,
            SHOW_PROJECT_BY_ID, SHOW_PROJECT_BY_INDEX,
            UPDATE_PROJECT_BY_ID, UPDATE_PROJECT_BY_INDEX,

            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            REMOVE_TASK_BY_ID, REMOVE_TASK_BY_INDEX,
            SHOW_TASK_BY_ID, SHOW_TASK_BY_INDEX,
            UPDATE_TASK_BY_ID, UPDATE_TASK_BY_INDEX,

            EXIT
    };

    @Override
    public Command[] getTerminalCommand() {
        return TERMINAL_COMMANDS;
    }

}
